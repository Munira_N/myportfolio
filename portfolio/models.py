from django.db import models

class Portfolio(models.Model):
    image = models.ImageField(upload_to='portfolio/images')
    title = models.CharField(max_length=100)
    description = models.TextField()
    link = models.URLField(blank=True)
