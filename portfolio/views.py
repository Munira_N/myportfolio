from django.shortcuts import render
from django.http import HttpResponse
from .models import Portfolio

def index(request):
    projects = Portfolio.objects.all()
    return render(request, 'portfolio/index.html', {'projects':projects} )
def base(request):
    return render(request, 'portfolio/base.html')
def contact(request):
    return render(request, 'portfolio/contact.html')

